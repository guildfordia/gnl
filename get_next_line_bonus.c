/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_bonus.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/27 14:42:39 by alangloi          #+#    #+#             */
/*   Updated: 2020/02/28 15:24:21 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line_bonus.h"

static int		init_gnl(int fd, char **mem, char **line)
{
	if (fd < 0 || !line || (read(fd, 0, 0)) < 0)
		return (-1);
	if (!*mem)
	{
	  if (!(*mem = malloc(sizeof(char) * BUFFER_SIZE + 1)))
			return (-1);
	}
	return (0);
}

static void		read_line(int fd, char **mem)
{
	char		buf[BUFFER_SIZE + 1];
	char		*tmp;
	int			ret;

	while ((ret = read(fd, buf, BUFFER_SIZE)) > 0)
	{
		buf[ret] = '\0';
		tmp = ft_strjoin(*mem, buf);
		free(*mem);
		*mem = tmp;
		if ((ft_strchr(*mem, '\n')))
			break ;
	}
}

int				get_next_line(int fd, char **line)
{
	static char	*mem;
	char		*tmp;
	int			i;

	if ((init_gnl(fd, &mem, line)) < 0)
		return (-1);
	read_line(fd, &mem);
	i = 0;
	while (mem[i] != '\n' && mem[i] != '\0')
		i++;
	if (mem)
		*line = ft_substr(mem, 0, i);
	if (!(ft_strchr(mem, '\n')))
	{
		free(mem);
		mem = NULL;
		return (0);
	}
	else
	{
		tmp = ft_strdup(&mem[i + 1]);
		free(mem);
		mem = tmp;
	}
	return (1);
}
